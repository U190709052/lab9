package ThreeDimensionalShape;

import workp.Shape;

public class Tetrahedron extends Shape {

    public double triangle_side;

    public Tetrahedron(double triangle_side) {
        this.triangle_side = triangle_side;
    }

    public double area(){
        return triangle_side*triangle_side*Math.sqrt(3);

    }

    public double volume(){
        return triangle_side*triangle_side*triangle_side*Math.sqrt(2)/12;
    }

    public double perimeter(){
        return triangle_side*6;
    }

    @Override
    public String toString() {
        return "Tetrahedron{" + "side=" + triangle_side + '}';
    }
}