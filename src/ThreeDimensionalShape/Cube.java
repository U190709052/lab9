package ThreeDimensionalShape;


import TwoDimensionalShape.Square;

public class Cube extends Square {

    public Cube(double square_side) {
        super(square_side);
    }


    public double area(){ return super.area()*6;
    }
    public double perimeter() {
        return (super.perimeter() * 3);
    }

    public double volume()
    {
        return (super.area()*square_side);
    }

    @Override
    public String toString(){
        return "side of cube: "+square_side;
    }


}