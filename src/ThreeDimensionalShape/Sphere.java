package ThreeDimensionalShape;

import TwoDimensionalShape.Circle;

public class Sphere extends Circle {

    public Sphere(double radius) {
        super(radius);

    }
    public double area(){
        return 4*Math.PI*circle_radius*circle_radius;
    }
    public double volume(){
        return Math.PI*circle_radius*circle_radius*circle_radius*4/3;
    }

    public double perimeter(){
        return 2*Math.PI*circle_radius;
    }


    @Override
    public String toString() {
        return "Sphere{" +
                "radius=" + circle_radius +
                '}';
    }
}