package TwoDimensionalShape;

import workp.Shape;

public class Circle extends Shape {

    public double circle_radius;

    public Circle(double radius) {
        this.circle_radius = circle_radius;
    }
    public double perimeter(){
        return Math.PI*2*circle_radius;

    }

    public double area(){
        return Math.PI*circle_radius*circle_radius;
    }




    @Override
    public String toString(){

        return "radius of circle : "+circle_radius;
    }
}