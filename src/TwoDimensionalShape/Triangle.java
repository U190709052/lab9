package TwoDimensionalShape;

import workp.Shape;

public class Triangle extends Shape {



    public double triangle_taban;
    public double triangle_side_2;
    public double triangle_side_3;
    public double Triangle_height;

    public Triangle(double triangle_taban, double triangle_side_2, double triangle_side_3, double Triangle_height) {
        this.triangle_taban = triangle_taban;
        this.triangle_side_2 = triangle_side_2;
        this.triangle_side_3 = triangle_side_3;
        this.Triangle_height = Triangle_height;

    }

    public double perimeter() {
        return triangle_taban+triangle_side_2+triangle_side_3 ;

    }
    public double area(){
        return (triangle_taban*Triangle_height)/2;
    }


    @Override
    public String toString() {
        return "Triangle{" +
                "lengthOfTheTriangle_taban=" + triangle_taban +
                ", lengthOfTheTriangle_side_2=" + triangle_side_2 +
                ", lengthOfTheTriangle_side_3=" + triangle_side_3 +
                ", heightOfTheTriangle=" + Triangle_height +
                '}';
    }
}