package TwoDimensionalShape;


import workp.Shape;

public class Square extends Shape {

    public double square_side;

    public Square(double square_side) {
        this.square_side = square_side;


    }

    public double perimeter(){
        return square_side*4;

    }


    public double area(){
        return square_side*square_side;
    }

    @Override
    public String toString(){
        return "side: "+square_side;
    }

}